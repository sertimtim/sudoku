#include "MainWindow.h"
#include "WorkSpace.h"
#include "SudokuSolution.h"
#include "MenuBar.h"
#include "Feedback.h"
#include <QColorDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QDesktopServices>
#include <QStatusBar>
#include <QKeyEvent>
#include <QLayout>
#include <QMenuBar>
#include <QLabel>
#include <QPushButton>
#include <QTextEdit>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{   
    setWindowIcon(QIcon("../Doc/Icon/about1.png"));
    setWindowTitle("Sudoku 2.2!");
    setFixedSize(550, 410);

    sd_current = new Sudo;
    sd_initial = new Sudo;

    ws = new WorkSpace;
    QVBoxLayout* ws_layout = new QVBoxLayout;
    ws_layout->addWidget(ws);
    QWidget* ws_widget = new QWidget;
    ws_widget->setLayout(ws_layout);
    setCentralWidget(ws_widget);

    mb = new MenuBar;
    menuBar()->addMenu(mb->mFile());
    menuBar()->addMenu(mb->mFormat());
    menuBar()->addMenu(mb->mHelp());

    addToolBar(Qt::TopToolBarArea, mb->toolBar());

    sb_lbl = new QLabel;
    statusBar()->addWidget(sb_lbl);

    menuNew();    
//Для меню
    connect(mb->aExit(), &QAction::triggered, this, &MainWindow::close);
    connect(mb->aNew(), &QAction::triggered, this, &MainWindow::menuNew);
    connect(mb->aSave(), &QAction::triggered, this, &MainWindow::menuSave);
    connect(mb->aOpen(), &QAction::triggered, this, &MainWindow::menuOpen);
    connect(mb->aColorField1(), &QAction::triggered, this, &MainWindow::menuColor);
    connect(mb->aColorField2(), &QAction::triggered, this, &MainWindow::menuColor);
    connect(mb->aAbout(), &QAction::triggered, this, &MainWindow::menuAbout);
    connect(mb->aComment(), &QAction::triggered, this, &MainWindow::menuComment);

//Для кнопок справа
    connect(ws->pbCom()->start(), &QPushButton::clicked, this, &MainWindow::start);
    connect(ws->clrAll(), &QPushButton::clicked, this, &MainWindow::clrAll);
    connect(ws->pbCom()->putAll(), &QPushButton::clicked, this, &MainWindow::putAll);
    connect(ws->pbCom()->showOne(), &QPushButton::toggled, this, &MainWindow::showOne);
    connect(ws->pbCom()->leShowOne(), &QLineEdit::textChanged, this, &MainWindow::vTextChange);
    for(size_t i=0; i<9; i++)
        for(size_t j=0; j<9; j++)
        {
            connect(ws->grIn()->getCell(i, j), &GridLineEdit::textChanged, this, &MainWindow::vTextChange);
            connect(ws->grIn()->getCell(i, j), &GridLineEdit::focusChanged, this, &MainWindow::setCellNumber);
        }
}

void MainWindow::keyPressEvent(QKeyEvent* pe)
{
    size_t i = 0, j = 0;
    for(i=0; i<9; i++)
        for(j=0; j<9; j++)
            if(ws->grIn()->getCell(i, j)->get_p())
            {
                if(pe->modifiers() & Qt::AltModifier)
                {
                    switch (pe->key())
                    {
                    case Qt::Key_Up:
                    {
                        if(i) i--;
                        break;
                    }
                    case Qt::Key_Down:
                    {
                        if(i<8) i++;
                        break;
                    }
                    case Qt::Key_Left:
                    {
                        if(j) j--;
                        break;
                    }
                    case Qt::Key_Right:
                    {
                        if(j<8) j++;
                        break;
                    }
                    default: QMainWindow::keyPressEvent(pe);
                    }
                    ws->grIn()->getCell(i, j)->setFocus();
                }
            }
}

void MainWindow::_start()
{
    ws->pbCom()->teComment()->clear();
    ws->pbCom()->start()->setDisabled(true);
    ws->pbCom()->putAll()->setDisabled(false);
    ws->pbCom()->putOne()->setDisabled(false);
    ws->pbCom()->showOne()->setDisabled(false);
    ws->pbCom()->leShowOne()->setDisabled(false);
    mb->aNew()->setDisabled(false);
    mb->aSave()->setDisabled(false);
}

void MainWindow::start()    // При нажатии на кнопку старт
{
    _start();

    for(size_t i=0; i<9; i++)
        for(size_t j=0; j<9; j++)
            if( !(ws->grIn()->getCell(i, j)->text().isEmpty()) )
            {
                QPalette old_pal = ws->grIn()->getCell(i, j)->palette();
                sd_current->getMas(i, j) = ws->grIn()->getCell(i, j)->text().toInt();
                sd_initial->getMas(i, j) = ws->grIn()->getCell(i, j)->text().toInt();
                ws->grIn()->getCell(i, j)->setReadOnly(true);
                ws->grIn()->getCell(i, j)->setStyleSheet(styleSheet().append("QLineEdit[readOnly=\"true\"] {font: bold}"));
                ws->grIn()->getCell(i, j)->setPalette(old_pal);
            }
}

void MainWindow::menuNew()  //Меню - New
{
    ws->pbCom()->leShowOne()->clear();
    ws->pbCom()->start()->setDisabled(false);
    ws->pbCom()->putAll()->setDisabled(true);
    ws->pbCom()->putOne()->setDisabled(true);
    ws->pbCom()->showOne()->setChecked(false);
    ws->pbCom()->showOne()->setDisabled(true);
    ws->pbCom()->leShowOne()->setDisabled(true);
    ws->pbCom()->teComment()->setText("Enter initial condition and push Start!\n*You can switch between cells using Alt+(Up/Down/Left/Right)");
    mb->aNew()->setDisabled(true);
    mb->aSave()->setDisabled(true);

    for(size_t i=0; i<9; i++)
        for(size_t j=0; j<9; j++)
            {
                QPalette old_pal = ws->grIn()->getCell(i, j)->palette();
                old_pal.setColor(QPalette::Text, Qt::black);
                sd_current->getMas(i, j) = 0;
                sd_initial->getMas(i, j) = 0;
                ws->grIn()->getCell(i, j)->setReadOnly(false);
                ws->grIn()->getCell(i, j)->clear();
                ws->grIn()->getCell(i, j)->setStyleSheet("QLineEdit[readOnly=\"false\"] {font: normal}");
                ws->grIn()->getCell(i, j)->setPalette(old_pal);
            }
}

void MainWindow::menuSave()
{
    QString file_name = QFileDialog::getSaveFileName(this, tr("Save file"), "../Doc/MySudoku");

    if(!file_name.isEmpty())
    {
        QFile out(file_name);
        if(out.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream writeStr(&out);
            writeStr << "#CurrentSudoku" << endl;
            for(size_t i=0; i<9; i++)
            {
                for(size_t j=0; j<9; j++)
                {
                    writeStr << ws->grIn()->getCell(i, j)->text().toInt();
                    if(j<8) writeStr << ' ';
                }
                writeStr << endl;
            }
            writeStr << "#InitialSudoku" << endl;
            for(size_t i=0; i<9; i++)
            {
                for(size_t j=0; j<9; j++)
                {
                    writeStr << sd_initial->getMas(i, j);
                    if(j<8) writeStr << ' ';
                }
                writeStr << endl;
            }
            out.close();
        }
    }

}

void MainWindow::menuOpen()
{
    QString file_name = QFileDialog::getOpenFileName(this, tr("Open file"), "../Doc/");

    if(!file_name.isEmpty())
    {
        menuNew();

        QFile in(file_name);
        if(in.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream readStr(&in);
            QString str;
            while (!readStr.atEnd())
            {
                str = readStr.readLine();
                if(str == "#CurrentSudoku")
                {
                    for(size_t i=0; i<9; i++)
                        for(size_t j=0; j<9; j++)
                            readStr >> sd_current->getMas(i, j);
                }

                if(str == "#InitialSudoku")
                {
                    for(size_t i=0; i<9; i++)
                        for(size_t j=0; j<9; j++)
                            readStr >> sd_initial->getMas(i, j);
                }
            }
            in.close();
        }

        for(size_t i=0; i<9; i++)
            for(size_t j=0; j<9; j++)
            {
                if(sd_initial->getMas(i, j) != 0)
                {
                    QPalette old_pal = ws->grIn()->getCell(i, j)->palette();
                    ws->grIn()->getCell(i, j)->setText(QString::number(sd_initial->getMas(i, j)));
                    ws->grIn()->getCell(i, j)->setReadOnly(true);
                    ws->grIn()->getCell(i, j)->setStyleSheet("QLineEdit[readOnly=\"true\"] {font: bold}");
                    ws->grIn()->getCell(i, j)->setPalette(old_pal);
                }
                if( (sd_current->getMas(i, j) != 0) && (sd_initial->getMas(i, j) == 0) )
                    ws->grIn()->getCell(i, j)->setText(QString::number(sd_current->getMas(i, j)));
            }

        _start();
    }
}

void MainWindow::menuColor()
{
    QColor color = QColorDialog::getColor(Qt::lightGray, this);

    //if ( ! sender() )
    //    abort();

    if ( (color.isValid()) && (sender()->objectName() == "Field1") )
    {
        ws->pbCom()->teComment()->setFocus();
        ws->grIn()->setPal(0, 0, color);
        ws->grIn()->setPal(0, 6, color);
        ws->grIn()->setPal(3, 3, color);
        ws->grIn()->setPal(6, 0, color);
        ws->grIn()->setPal(6, 6, color);
    }

    if ( (color.isValid()) && (sender()->objectName() == "Field2") )
    {
        ws->pbCom()->teComment()->setFocus();
        ws->grIn()->setPal(0, 3, color);
        ws->grIn()->setPal(3, 0, color);
        ws->grIn()->setPal(3, 6, color);
        ws->grIn()->setPal(6, 3, color);
    }
}

void MainWindow::menuAbout()
{
    QString str;
    QFile f_about("../Doc/Text/AboutHTML.html");
    if(f_about.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream readStr(&f_about);
        str = readStr.readAll();
        f_about.close();
    }    

    QMessageBox* m_mb = new QMessageBox(QMessageBox::NoIcon, "About Sudoku", str, QMessageBox::Ok, this);
    m_mb->setIconPixmap(QPixmap("../Doc/Icon/about1.png"));
    m_mb->exec();
}

void MainWindow::menuComment()
{
    Feedback* m_fb = new Feedback(this);
    m_fb->setWindowTitle("Feedback!");
    if(m_fb->exec() == QDialog::Accepted)
    {        
        QDesktopServices::openUrl(QUrl("http://www.mail.ru"));
        QDesktopServices::openUrl(QUrl(QDir::currentPath()));
    }
}

void MainWindow::clrAll()   // Очищает все кроме начальных условий
{
    for(size_t i=0; i<9; i++)
        for(size_t j=0; j<9; j++)
            if(sd_initial->getMas(i, j) == 0)
            {
                ws->grIn()->getCell(i, j)->clear();
            }
}

void MainWindow::enterMas() // Обнуляет и считывает текущее судоку
{
    for(int i=0; i<9; i++)
        for(int j=0; j<9; j++)
            sd_current->getMas(i, j) = 0;

    for(size_t i=0; i<9; i++)
        for(size_t j=0; j<9; j++)
            if( !(ws->grIn()->getCell(i, j)->text().isEmpty()) )
                sd_current->getMas(i, j) = ws->grIn()->getCell(i, j)->text().toInt();
}

void MainWindow::putAll()   // Раскладывает судоку
{
    ws->pbCom()->showOne()->setChecked(false);
    enterMas();

    int count = 0;
    while ((sd_current->exit()) && (count != 20))
    {
        sd_current->line();
        sd_current->column();
        sd_current->square();
        count++;
    }

    for(size_t i=0; i<9; i++)
        for(size_t j=0; j<9; j++)
            if( (ws->grIn()->getCell(i, j)->text().isEmpty()) || (ws->grIn()->getCell(i, j)->text().toInt() == 0) )
                ws->grIn()->getCell(i, j)->setText(QString::number(sd_current->getMas(i, j)));
}

void MainWindow::showOne()  // Показывает цифру красным плейсхолдером
{
    if(ws->pbCom()->showOne()->isChecked())
    {
        ws->pbCom()->showOne()->setText("Hide one");
        clrPlaceholder();
        if( !(ws->pbCom()->leShowOne()->text().isEmpty()) )
        {
            ws->pbCom()->teComment()->clear();
            int x = ws->pbCom()->leShowOne()->text().toInt();
            enterMas();

            for(size_t i=0; i<9; i++)
                for(size_t j=0; j<9; j++)
                    if(sd_current->trueOrFalse(x, i, j) && (sd_current->getMas(i, j) == 0))
                    {
                        QPalette old_pal = ws->grIn()->getCell(i, j)->palette();
                        old_pal.setColor(QPalette::Text, Qt::red);
                        ws->grIn()->getCell(i, j)->setPlaceholderText(QString::number(x));
                        ws->grIn()->getCell(i, j)->setStyleSheet("QLineEdit { color: red }");
                        ws->grIn()->getCell(i, j)->setPalette(old_pal);
                    }
        }
        else
        {
            ws->pbCom()->teComment()->setText("Enter a number [1 - 9]!");
        }
    }
    else
    {
        ws->pbCom()->showOne()->setText("Show one");
        clrPlaceholder();
        ws->pbCom()->teComment()->clear();
    }
}

void MainWindow::clrPlaceholder()   // Удаляет весь плейсхолдер и ставит цвет текста черный
{
    for(size_t i=0; i<9; i++)
    for(size_t j=0; j<9; j++)
    {
        if(sd_initial->getMas(i, j) == 0)
        {
            QPalette old_pal = ws->grIn()->getCell(i, j)->palette();
            ws->grIn()->getCell(i, j)->setPlaceholderText("");
            ws->grIn()->getCell(i, j)->setStyleSheet("QLineEdit { color: black }");
            ws->grIn()->getCell(i, j)->setPalette(old_pal);
        }
    }
}

void MainWindow::vTextChange()  // При изменении текста в поле и в LineEdit
{
    if(ws->pbCom()->showOne()->isChecked())
        showOne();    
}

void MainWindow::setCellNumber()    // Строка состояния
{
    sb_lbl->setText("");

    for(size_t i=0; i<9; i++)
        for(size_t j=0; j<9; j++)
            if(ws->grIn()->getCell(i, j)->get_p())
            {
                sb_lbl->setText("Cell: " + QString::number(i+1) + ", " + QString::number(j+1));
                break;
            }
}
