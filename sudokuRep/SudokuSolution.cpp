#include "SudokuSolution.h"
#include <algorithm>
using std::vector;

Sudo::Sudo()
{
    for(int i=0; i<9; i++)
        for(int j=0; j<9; j++)
            mas[i][j] = 0;
}

bool Sudo::trueOrFalse(int x, int i, int j)   // Подходит х или нет
{
    bool p0;

    p0 = true;
    switch (j)
    {
    case 0:
    case 1:
    case 2:
        if(i<3) p0 = trueOrFalseSqr(x, 0, 0);
        else if(i>2 && i<6) p0 = trueOrFalseSqr(x, 3, 0);
        else if(i>5) p0 = trueOrFalseSqr(x, 6, 0);
        break;
    case 3:
    case 4:
    case 5:
        if(i<3) p0 = trueOrFalseSqr(x, 0, 3);
        else if(i>2 && i<6) p0 = trueOrFalseSqr(x, 3, 3);
        else if(i>5) p0 = trueOrFalseSqr(x, 6, 3);
        break;
    case 6:
    case 7:
    case 8:
        if(i<3) p0 = trueOrFalseSqr(x, 0, 6);
        else if(i>2 && i<6) p0 = trueOrFalseSqr(x, 3, 6);
        else if(i>5) p0 = trueOrFalseSqr(x, 6, 6);
        break;
    }

    if(p0)  // по столбцу
    {
        for(int k=0; k<9; k++)
            if(x == mas[k][j]) {p0 = false; break;}
    }

    if(p0)  // по строке
    {
     for(int k=0; k<9; k++)
        if(x == mas[i][k]) {p0 = false; break;}
    }
    return p0;
}

bool Sudo::trueOrFalseSqr(int x, int q, int w) // Подфункция - Пробегает по полю 3х3 и ищет совпадение с исходным массивом
{
    bool p0 = true;
    for(int i=q; i<q+3; i++)
        for(int j=w; j<w+3; j++)
            if(x == mas[i][j]) p0 = false;
    return p0;
}

void Sudo::line()
{
    vector<int> v;
    int i, j;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            if(mas[i][j] == 0)
            {
                for(int k=1; k<10; k++)
                {
                    if(trueOrFalse(k, i, j))
                    {
                        v.push_back(k);
                    }
                }
            }
        }
        inputLine(v, i);
        v.clear();
    }
}

inline void Sudo::inputLine(const vector<int> &v, int i)
{
    int n;
    for(int k=1; k<10; k++)
    {
        n = count(v.begin(), v.end(), k);
        if(n == 1)
        {
            for(int j=0; j<9; j++)
            {
                if((trueOrFalse(k, i, j)) && (mas[i][j] == 0))
                {
                    mas[i][j] = k;
                }
            }
        }
    }
}

void Sudo::column()
{
    vector<int> v;
    int i, j;
    for(j=0; j<9; j++)
    {
        for(i=0; i<9; i++)
        {
            if(mas[i][j] == 0)
            {
                for(int k=1; k<10; k++)
                {
                    if(trueOrFalse(k, i, j))
                    {
                        v.push_back(k);
                    }
                }
            }
        }
        inputColumn(v, j);
        v.clear();
    }
}

inline void Sudo::inputColumn(const vector<int> &v, int j)
{
    int n;
    for(int k=1; k<10; k++)
    {
        n = count(v.begin(), v.end(), k);
        if(n == 1)
        {
            for(int i=0; i<9; i++)
            {
                if((trueOrFalse(k, i, j)) && (mas[i][j] == 0))
                {
                    mas[i][j] = k;
                }
            }
        }
    }
}

void Sudo::square()
{
    int i, j;
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            switch (j)
            {
            case 0:
                if(i==0) inputSquare(0, 0);
                else if(i==1) inputSquare(3, 0);
                else if(i==2) inputSquare(6, 0);
                break;
            case 1:
                if(i==0) inputSquare(0, 3);
                else if(i==1) inputSquare(3, 3);
                else if(i==2) inputSquare(6, 3);
                break;
            case 2:
                if(i==0) inputSquare(0, 6);
                else if(i==1) inputSquare(3, 6);
                else if(i==2) inputSquare(6, 6);
                break;
            }
        }
    }
}

void Sudo::inputSquare(int q, int w)
{
    vector<int> v;
    for(int i=q; i<q+3; i++)
    {
        for(int j=w; j<w+3; j++)
        {
            if(mas[i][j] == 0)
            {
                for(int k=1; k<10; k++)
                {
                    if(trueOrFalse(k, i, j))
                    {
                        v.push_back(k);
                    }
                }
            }
        }
    }
    int n;
    for(int k=1; k<10; k++)
    {
        n = count(v.begin(), v.end(), k);
        if(n == 1)
        {
            for(int i=q; i<q+3; i++)
            {
                for(int j=w; j<w+3; j++)
                {
                    if((trueOrFalse(k, i, j)) && (mas[i][j] == 0))
                    {
                        mas[i][j] = k;
                    }
                }
            }
        }
    }
    v.clear();
}

bool Sudo::exit()
{
    bool p = false;
    for(int i=0; i<9; i++)
        for(int j=0; j<9; j++)
            if(mas[i][j] == 0) p = true;
    return p;
}
