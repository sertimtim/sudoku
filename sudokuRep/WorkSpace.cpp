#include "WorkSpace.h"
#include <QLabel>
#include <QPushButton>
#include <QLayout>
#include <QSplitter>

WorkSpace::WorkSpace(QWidget *parent) : QWidget(parent)
{
    gr_in = new Grid;

    QLabel* lbl_ic = new QLabel("Current Sudoku:");
    pb_clrAll = new QPushButton(QPixmap("../Doc/Icon/clear.png"), "Clear");
    pb_clrAll->setFlat(true);
    pb_clrAll->setStyleSheet("QPushButton:hover{background: rgb(255,255,255,150); border: none;}"
                             "QPushButton:pressed{background: rgb(192,192,192,150); border: none;}"
                             );//"QPushButton {border-radius: 5px;}"  min-width: 50px; border: 1px solid white;
    pb_clrAll->setCursor(Qt::PointingHandCursor);
    pb_clrAll->setToolTip("Clear all but the initial conditions");

    QHBoxLayout* lo_clrAll = new QHBoxLayout;
    lo_clrAll->addWidget(lbl_ic, 1);
    lo_clrAll->addWidget(pb_clrAll);

    QVBoxLayout* lo_grd = new QVBoxLayout;
    lo_grd->addLayout(lo_clrAll);
    lo_grd->addWidget(gr_in);
    QSplitter* spl_v = new QSplitter(Qt::Vertical);
    lo_grd->addWidget(spl_v);

    pb_com = new PBCommand;

    QSplitter* spl_h = new QSplitter(Qt::Horizontal);
    QHBoxLayout* lo_all = new QHBoxLayout;
    lo_all->addLayout(lo_grd);
    lo_all->addWidget(spl_h);
    lo_all->addWidget(pb_com);
    lo_all->addStretch(1);

    setLayout(lo_all);
}
