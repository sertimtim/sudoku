#ifndef GRIDLINEEDIT_H
#define GRIDLINEEDIT_H

#include <QLineEdit>

class GridLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    GridLineEdit(QWidget* parent = nullptr);
    bool get_p() const { return p; }

protected:
    void focusInEvent(QFocusEvent* pe) override;
    void focusOutEvent(QFocusEvent* pe) override;

signals:
    void focusChanged();

private:
    QPalette old_pal;
    bool p;
};

#endif // GRIDLINEEDIT_H
