#include "Feedback.h"
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QLayout>
#include <QSpinBox>
#include <QPushButton>
#include <QDateTimeEdit>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

Feedback::Feedback(QWidget *parent) : QDialog(parent)
{
    QLabel* lbl_fam = new QLabel("Family: ");
    QLabel* lbl_name = new QLabel("Name: ");
    QLabel* lbl_prg = new QLabel("Programm: ");

    le_fam = new QLineEdit;
    le_fam->setPlaceholderText("Enter your family");
    le_name = new QLineEdit;
    le_name->setPlaceholderText("Enter your name");
    le_prg = new QLineEdit;
    le_prg->setPlaceholderText("Enter programm name and version");

    QRegExp rxp("^\\w{1}[a-z]{20}$"); //[1-9]{1,1}[0-9]{2}
    QValidator* vld = new QRegExpValidator(rxp);
    le_fam->setValidator(vld);
    le_name->setValidator(vld);

    QHBoxLayout* lo_fam = new QHBoxLayout;
    lo_fam->addWidget(lbl_fam);
    lo_fam->addWidget(le_fam);
    QHBoxLayout* lo_name = new QHBoxLayout;
    lo_name->addWidget(lbl_name);
    lo_name->addWidget(le_name);
    QHBoxLayout* lo_prg = new QHBoxLayout;
    lo_prg->addWidget(lbl_prg);
    lo_prg->addWidget(le_prg);

    QLabel* lbl_rtng = new QLabel("\nEnter ratings (1 to 5) for the following characteristics:");
    QLabel* lbl_dsg = new QLabel("Design: ");
    QLabel* lbl_usb = new QLabel("Usability: ");
    QLabel* lbl_func = new QLabel("Functionality: ");

    sb_dsg = new QSpinBox;
    sb_dsg->setRange(1, 5);
    sb_dsg->setWrapping(true);
    sb_usb = new QSpinBox;
    sb_usb->setRange(1, 5);
    sb_usb->setWrapping(true);
    sb_func = new QSpinBox;
    sb_func->setRange(1, 5);
    sb_func->setWrapping(true);

    QHBoxLayout* lo_dsg = new QHBoxLayout;
    lo_dsg->addWidget(lbl_dsg);
    lo_dsg->addWidget(sb_dsg);
    QHBoxLayout* lo_usb = new QHBoxLayout;
    lo_usb->addWidget(lbl_usb);
    lo_usb->addWidget(sb_usb);
    QHBoxLayout* lo_func = new QHBoxLayout;
    lo_func->addWidget(lbl_func);
    lo_func->addWidget(sb_func);

    QLabel* lbl_cmm = new QLabel("&Comment:");
    te = new QTextEdit;
    lbl_cmm->setBuddy(te);

    pb_back = new QPushButton("Cancel");
    pb_rprt = new QPushButton("Report");
    QHBoxLayout* lo_pb = new QHBoxLayout;
    lo_pb->addWidget(pb_back);
    lo_pb->addWidget(pb_rprt);
    connect(pb_back, &QPushButton::clicked, this, &QDialog::reject);
    connect(pb_rprt, &QPushButton::clicked, this, &Feedback::createReport);

    QVBoxLayout* lo_comm = new QVBoxLayout;
    lo_comm->addLayout(lo_name);
    lo_comm->addLayout(lo_fam);
    lo_comm->addLayout(lo_prg);
    lo_comm->addWidget(lbl_rtng);
    lo_comm->addLayout(lo_dsg);
    lo_comm->addLayout(lo_usb);
    lo_comm->addLayout(lo_func);
    lo_comm->addWidget(lbl_cmm);
    lo_comm->addWidget(te);
    lo_comm->addLayout(lo_pb);

    setLayout(lo_comm);
}

void Feedback::createReport()
{
    if( le_name->text().isEmpty() || le_fam->text().isEmpty() || le_prg->text().isEmpty() )
    {
        QMessageBox::information(this, "Information", "Enter all fields");
    }
    else
    {
        QString name, fam, prg, comm, time;
        int dsg, usb, func;
        name = le_name->text();
        fam = le_fam->text();
        prg = le_prg->text();
        comm = te->toPlainText();
        dsg = sb_dsg->text().toInt();
        usb = sb_usb->text().toInt();
        func = sb_func->text().toInt();

        QDateTimeEdit* t = new QDateTimeEdit(QDateTime::currentDateTime());
        time = t->text();

        QFile out("report");
        if(out.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream writeStr(&out);
            fam[0] = fam[0].toUpper();
            name[0] = name[0].toUpper();
            writeStr << fam << ' ' << name << " about " << prg << endl;
            writeStr << "Design: " << dsg << endl;
            writeStr << "Usability: " << usb << endl;
            writeStr << "Functionality: " << func << endl;
            writeStr << "Comment: " << comm << "\n";
            writeStr << "Current time: " << time << "\n\n";
            out.close();
        }
        QDialog::accept();
    }
}
