#include "PBCommand.h"
#include <QLayout>
#include <QValidator>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QSplitter>

PBCommand::PBCommand(QWidget *parent): QWidget(parent)
{
    QRegExp rxp("[1-9]\\d{0,0}");
    QValidator* vld_sOne = new QRegExpValidator(rxp);

    pb_start = new QPushButton("Start");
    pb_putAll = new QPushButton("Put all");
    pb_putOne = new QPushButton("Put one");
    pb_showOne = new QPushButton("Show one");
    pb_showOne->setCheckable(true);
    QLabel* lbl_showOne = new QLabel("Enter number:");
    QLabel* lbl_comm = new QLabel("Comment:");
    le_showOne = new QLineEdit;
    le_showOne->setValidator(vld_sOne);
    le_showOne->setFixedWidth(50);
    le_showOne->setStyleSheet("QLineEdit { color: red }");
    te_comment = new QTextEdit;
    te_comment->setReadOnly(true);

    QHBoxLayout* lo_showOne = new QHBoxLayout;
    lo_showOne->addWidget(lbl_showOne);
    lo_showOne->addWidget(le_showOne);
    lo_showOne->addStretch(1);
    lo_showOne->addWidget(pb_showOne);

    QVBoxLayout* lo_butt = new QVBoxLayout;
    lo_butt->addWidget(pb_start);
    lo_butt->addWidget(pb_putAll);
    lo_butt->addWidget(pb_putOne);
    lo_butt->addLayout(lo_showOne);
    lo_butt->addWidget(lbl_comm);
    lo_butt->addWidget(te_comment);

    QSplitter* spl_v = new QSplitter(Qt::Vertical);
    lo_butt->addWidget(spl_v);

    setLayout(lo_butt);
}
