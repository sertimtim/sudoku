#ifndef MENUBAR_H
#define MENUBAR_H

#include <QWidget>

class QMenu;
class QToolBar;

class MenuBar : public QWidget
{
public:
    explicit MenuBar(QWidget* parent = nullptr);

    QMenu* mFile() const { return m_file; }
    QMenu* mFormat() const { return m_format; }
    QMenu* mColor() const { return mm_color; }
    QMenu* mHelp() const { return m_help; }

    QAction* aNew() const { return a_new; }
    QAction* aSave() const { return a_save; }
    QAction* aOpen() const { return a_open; }
    QAction* aExit() const { return a_exit; }
    QAction* aColorField1() const { return a_colorField1; }
    QAction* aColorField2() const { return a_colorField2; }
    QAction* aAbout() const { return a_about; }
    QAction* aComment() const { return a_feedback; }

    QToolBar* toolBar() const { return tb; }

private:
    QMenu* m_file;
    QAction* a_new;
    QAction* a_save;
    QAction* a_open;
    QAction* a_exit;

    QMenu* m_format;
    QMenu* mm_color;
    QAction* a_colorField1;
    QAction* a_colorField2;

    QMenu* m_help;
    QAction* a_about;
    QAction* a_feedback;

    QToolBar* tb;
};

#endif // MENUBAR_H
