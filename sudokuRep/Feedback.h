#ifndef FEEDBACK_H
#define FEEDBACK_H

#include <QDialog>

class QLineEdit;
class QSpinBox;
class QTextEdit;

class Feedback : public QDialog
{
public:
    Feedback(QWidget *parent = nullptr);

    void createReport();

private:
    QPushButton* pb_back;
    QPushButton* pb_rprt;
    QLineEdit* le_fam;
    QLineEdit* le_name;
    QLineEdit* le_prg;
    QSpinBox* sb_dsg, * sb_usb, * sb_func;
    QTextEdit* te;
};
#endif // FEEDBACK_H
