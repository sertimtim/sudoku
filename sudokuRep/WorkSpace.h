#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <QWidget>
#include "Grid.h"
#include "PBCommand.h"

class QPushButton;

class WorkSpace : public QWidget
{
public:
    explicit WorkSpace(QWidget *parent = nullptr);

    Grid* grIn() const { return gr_in; }
    PBCommand* pbCom() const { return pb_com; }
    QPushButton* clrAll() const { return pb_clrAll; }

private:
    Grid* gr_in;
    PBCommand* pb_com;
    QPushButton* pb_clrAll;
};


#endif // WORKSPACE_H
