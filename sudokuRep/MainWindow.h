#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class Sudo;
class WorkSpace;
class MenuBar;
class QLabel;

class MainWindow : public QMainWindow
{    
public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    Sudo* sd_current, *sd_initial;
    WorkSpace* ws;
    MenuBar* mb;
    QLabel* sb_lbl;

protected:
    virtual void keyPressEvent(QKeyEvent* pe) override;

public slots:
    void _start();
    void start();
    void menuNew();
    void menuSave();
    void menuOpen();
    void menuColor();
    void menuAbout();
    void menuComment();
    void enterMas();
    void putAll();
    void clrAll();
    void showOne();
    void clrPlaceholder();
    void vTextChange();
    void setCellNumber();
};

#endif // MAINWINDOW_H
