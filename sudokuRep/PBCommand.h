#ifndef PBCOMMAND_H
#define PBCOMMAND_H

#include <QWidget>

class QLineEdit;
class QTextEdit;
class QPushButton;

class PBCommand : public QWidget
{
public:
    PBCommand(QWidget *parent = nullptr);

    QLineEdit* leShowOne() const { return le_showOne; }
    QTextEdit* teComment() const { return te_comment; }
    QPushButton* start() const { return pb_start; }
    QPushButton* putAll() const { return pb_putAll; }
    QPushButton* putOne() const { return pb_putOne; }
    QPushButton* showOne() const {return pb_showOne; }

private:
    QLineEdit* le_showOne;
    QTextEdit* te_comment;
    QPushButton* pb_start;
    QPushButton* pb_putAll;
    QPushButton* pb_putOne;
    QPushButton* pb_showOne;
};

#endif // PBCOMMAND_H
