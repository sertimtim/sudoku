#ifndef GRID_H
#define GRID_H

#include <QWidget>
#include "GridLineEdit.h"

class Grid : public QWidget
{

public:
    Grid(QWidget *parent = nullptr);

    void setPal(const size_t& q = 0, const size_t& w = 0, const QColor& color = Qt::lightGray);

    GridLineEdit* getCell(size_t i, size_t j) const { return cell[i][j]; }

private:
    std::vector<std::vector<GridLineEdit*>> cell;
};

#endif // GRID_H
