#include "Grid.h"
#include <QLayout>
#include <QValidator>

Grid::Grid(QWidget *parent): QWidget(parent)
{
    const int n = 9;

    QGridLayout* lo_t = new QGridLayout;

    QRegExp rxp("[1-9]\\d{0,0}");
    QValidator* vld_grid = new QRegExpValidator(rxp);

    cell.resize(n);
    for(size_t i=0; i<n; i++)
    {
        cell[i].resize(n);
        for(size_t j=0; j<n; j++)
        {
            cell[i][j] = new GridLineEdit;
            cell[i][j]->setFixedSize(25, 25);
            cell[i][j]->setAlignment(Qt::AlignCenter);
            cell[i][j]->setValidator(vld_grid);
            lo_t->addWidget(cell[i][j], i, j);
        }
    }
    lo_t->setSpacing(1);

    setPal(0, 0);
    setPal(0, 6);
    setPal(3, 3);
    setPal(6, 0);
    setPal(6, 6);

    setLayout(lo_t);
}

void Grid::setPal(const size_t& q, const size_t& w, const QColor& color)
{
    QPalette pal;
    pal.setColor(QPalette::Base, color);
    for(size_t i=q; i<q+3; i++)
        for(size_t j=w; j<w+3; j++)
        {
            cell[i][j]->setPalette(pal);
        }
}
