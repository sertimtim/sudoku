#ifndef SUDO_H
#define SUDO_H

#include <vector>

class Sudo
{
private:
    int mas[9][9];
public:
    Sudo();

    bool trueOrFalse(int x, int i, int j);
    bool trueOrFalseSqr(int x, int q, int w);
    void line();
    inline void inputLine(const std::vector<int> &v, int i);
    void column();
    inline void inputColumn(const std::vector<int> &v, int j);
    void square();
    void inputSquare(int q, int w);
    bool exit();

    int& getMas(size_t i, size_t j) { return mas[i][j]; }

    //int* mass() const
};

#endif // SUDO_H
