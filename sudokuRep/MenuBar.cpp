#include "MenuBar.h"
#include <QMenu>
#include <QToolBar>

MenuBar::MenuBar(QWidget* parent) : QWidget(parent)
{
    m_file = new QMenu("&File");
    a_new = new QAction("New");
    a_new->setShortcut(QKeySequence("CTRL+N"));
    a_new->setIcon(QPixmap("../Doc/Icon/new.png"));
    a_new->setToolTip("Create new Sudoku");
    a_save = new QAction("Save...");
    a_save->setShortcut(QKeySequence("CTRL+S"));
    a_save->setIcon(QPixmap("../Doc/Icon/save.png"));
    a_save->setToolTip("Save current Sudoku");
    a_open = new QAction("Open...");
    a_open->setShortcut(QKeySequence("CTRL+O"));
    a_open->setIcon(QPixmap("../Doc/Icon/open.png"));
    a_open->setToolTip("Open previously saved Sudoku");
    a_exit = new QAction("Exit");
    m_file->addAction(a_new);
    m_file->addAction(a_open);
    m_file->addAction(a_save);
    m_file->addSeparator();
    m_file->addAction(a_exit);

    mm_color = new QMenu("Color");
    a_colorField1 = new QAction("Field1");
    a_colorField1->setObjectName("Field1");
    a_colorField2 = new QAction("Field2");
    a_colorField2->setObjectName("Field2");
    a_colorField1->setIcon(QPixmap("../Doc/Icon/Field1.png"));
    a_colorField2->setIcon(QPixmap("../Doc/Icon/Field2.png"));
    mm_color->addAction(a_colorField1);
    mm_color->addAction(a_colorField2);
    m_format = new QMenu("&View");
    m_format->addMenu(mm_color);

    m_help = new QMenu("&Help");
    a_about = new QAction("About Sudoku");
    a_feedback = new QAction("Feedback...");
    m_help->addAction(a_about);
    m_help->addSeparator();
    m_help->addAction(a_feedback);
// Панель инструментов
    tb = new QToolBar("Linker toolbar");
    tb->addAction(a_new);
    tb->addAction(a_open);
    tb->addAction(a_save);
}
