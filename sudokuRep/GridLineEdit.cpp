#include "GridLineEdit.h"

GridLineEdit::GridLineEdit(QWidget* parent) : QLineEdit(parent),
    p(false)
{

}

void GridLineEdit::focusInEvent(QFocusEvent* pe)
{
    old_pal = this->palette();

    QLineEdit::focusInEvent(pe);
    QPalette new_pal;
    new_pal.setColor(QPalette::Base, QColor(0, 255, 0, 64));
    setPalette(new_pal);
    p = true;

    emit focusChanged();
}

void GridLineEdit::focusOutEvent(QFocusEvent* pe)
{
    QLineEdit::focusOutEvent(pe);
    setPalette(old_pal);
    p = false;

    emit focusChanged();
}
